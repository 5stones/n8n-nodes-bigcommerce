# [1.7.0](https://gitlab.com/5stones/n8n-nodes-bigcommerce/compare/v1.6.0...v1.7.0) (2024-07-22)


### Features

* **resources:** add subscribers with get all and get one by id ([f70016b](https://gitlab.com/5stones/n8n-nodes-bigcommerce/commit/f70016b68af8ece2df6d8f4170f2d2eedf5ab807))



# [1.6.0](https://gitlab.com/5stones/n8n-nodes-bigcommerce/compare/v1.5.0...v1.6.0) (2022-01-07)


### Bug Fixes

* **nodes:** error should be thrown when error comes up on bigcommerce node ([a6b8a4a](https://gitlab.com/5stones/n8n-nodes-bigcommerce/commit/a6b8a4ae155d2040ff6d2ccc6f2ed73d9ac1fe9e))


### Features

* **trigger:** add trigger node to manage bc webhooks ([04da0ef](https://gitlab.com/5stones/n8n-nodes-bigcommerce/commit/04da0ef9d5dbe4f1b00e8c603baae1c9b7161f27))



# [1.5.0](https://gitlab.com/5stones/n8n-nodes-bigcommerce/compare/v1.4.1...v1.5.0) (2021-11-01)


### Features

* **refunds:** add ability to create refunds and refund quotes ([240f6c3](https://gitlab.com/5stones/n8n-nodes-bigcommerce/commit/240f6c3b8726b0609fc863f78ae989447d60f727))



## [1.4.1](https://gitlab.com/5stones/n8n-nodes-bigcommerce/compare/v1.0.0...v1.4.1) (2021-10-18)


### Bug Fixes

* **nodes, resources, package:** fix errors from updated npm packages ([31cf90f](https://gitlab.com/5stones/n8n-nodes-bigcommerce/commit/31cf90fb915f27ea8c7f43803174e856698ea0a2))
* **order/execute:** move get order_id into operation thats use it ([1b66aa8](https://gitlab.com/5stones/n8n-nodes-bigcommerce/commit/1b66aa8d526305fe19b343bd40285b107c1c3c0a))


### Features

* **interfaces, resources:** update order create to allow for a array of json objects to create a itterable number of products ([6858e69](https://gitlab.com/5stones/n8n-nodes-bigcommerce/commit/6858e69431195d30c22ccca8ac8b5f0bed92c938))
* **nodes, resources:** add endpoint to get all variants ([b0e8e9f](https://gitlab.com/5stones/n8n-nodes-bigcommerce/commit/b0e8e9f92fa8c251cd81005b7e11dc69e596ffc3))
* **nodes, resources:** add get order shipments to node ([f317bd6](https://gitlab.com/5stones/n8n-nodes-bigcommerce/commit/f317bd66b224762037abd35f37720a22a113749f))
* **nodes, resources:** add product as a resource option for the bigcommerce node ([b99f5d8](https://gitlab.com/5stones/n8n-nodes-bigcommerce/commit/b99f5d815e75d4323770ea16be4d669beadbb9b8))
* **package:** update version ([3e5c3a9](https://gitlab.com/5stones/n8n-nodes-bigcommerce/commit/3e5c3a9267497589c3d0940f34624903344f0cee))



# [1.0.0](https://gitlab.com/5stones/n8n-nodes-bigcommerce/compare/3d43ad7610af69cad6fd2fef8405c8b0ea862246...v1.0.0) (2021-07-15)


### Bug Fixes

* **node-properties:** add upsert for customerFormFieldValues ([7c4e536](https://gitlab.com/5stones/n8n-nodes-bigcommerce/commit/7c4e536fd895438eb5ed2c7af62f70b28e68e3e6))
* **nodes:** remove uneeded slash ([ae4eaab](https://gitlab.com/5stones/n8n-nodes-bigcommerce/commit/ae4eaabac24380f4f7284c2b475b95e6ab59c897))


### Features

* add Bigcommerce node ([3d43ad7](https://gitlab.com/5stones/n8n-nodes-bigcommerce/commit/3d43ad7610af69cad6fd2fef8405c8b0ea862246))
* **Customer.ts, Bigcommerce.node:** add get contact group method ([913d534](https://gitlab.com/5stones/n8n-nodes-bigcommerce/commit/913d53444920f69cc327aa48196182952db6f059))
* **node-properties, nodes:** add customer address get call to custom node ([c85e7e8](https://gitlab.com/5stones/n8n-nodes-bigcommerce/commit/c85e7e8ea446a3b45405d730a195c50033a925e6))
* **node-properties, nodes:** add get lineitems to custom node ([4c83ebe](https://gitlab.com/5stones/n8n-nodes-bigcommerce/commit/4c83ebe9aa729d953af0b325781bd92a97ead600))
* **order:** update order resource to have opperation to get single order by id ([dcd318a](https://gitlab.com/5stones/n8n-nodes-bigcommerce/commit/dcd318aacc4dc5e2925bd43890885cb2fbf29f2d))
* **Refund, Bigcommerce.node:** get refunds from BC ([a713f8d](https://gitlab.com/5stones/n8n-nodes-bigcommerce/commit/a713f8db73710e67761d60ac490810be6b66737a))
* **resources, nodes:** add coupon resource to bigcommerce node ([1cefd26](https://gitlab.com/5stones/n8n-nodes-bigcommerce/commit/1cefd26d9bd1b104c72183646e02430c74c88e61))
* **src/nodes/Bigcommerce.node:** add error handling into bc node ([9c32301](https://gitlab.com/5stones/n8n-nodes-bigcommerce/commit/9c323016f67919428852b23f023b88c91d915fba))
* add order address resource ([93c3975](https://gitlab.com/5stones/n8n-nodes-bigcommerce/commit/93c3975d72c74252a0198c152301e3c57b08fd10))


### Reverts

* Revert "chore(src): abstract and clean up code" ([42b1887](https://gitlab.com/5stones/n8n-nodes-bigcommerce/commit/42b1887e416e516dd33bcff1e8a7512e05a8fca3))



