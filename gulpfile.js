const { src, dest } = require('gulp');

function copyIcons() {
	return src('src/**/*.png')
		.pipe(dest('dist/src'));
}

exports.default = copyIcons;
