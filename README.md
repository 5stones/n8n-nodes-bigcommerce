# n8n-nodes-bigcommerce

## Release

The standard release command for this project is:
```
npm version [<newversion> | major | minor | patch | premajor | preminor | prepatch | prerelease | from-git]
```

This command will:

1. Generate/update the Changelog
1. Bump the package version
1. Tag & pushing the commit
1. Publish the package to NPM (assuming you are logged in to NPM and have the correct access)

e.g.

```
npm version 1.2.17
npm version patch // 1.2.17 -> 1.2.18
```

## Dev

To run n8n with this package during development, the built package must be inside the n8n's node_modules:

```
docker run -p5678:5678 -v "$PWD":/usr/local/lib/node_modules/n8n/node_modules/@5stones/n8n-nodes-bigcommerce n8nio/n8n
```
