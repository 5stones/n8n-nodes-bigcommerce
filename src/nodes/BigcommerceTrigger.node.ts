import {
  IWebhookFunctions,
  IHookFunctions,
  IDataObject,
  INodeType,
  INodeTypeDescription,
  IWebhookResponseData,
} from 'n8n-workflow';

import { bigcommerceApi } from '../resources';

export class BigcommerceTrigger implements INodeType {
  description: INodeTypeDescription = {
    displayName: 'BigCommerce Trigger',
    name: 'bigcommerceTrigger',
    group: ['trigger'],
    version: 1,
    subtitle: '={{$parameter["events"]}}',
    description: 'Handle BigCommerce events via webhooks',
    defaults: {
      name: 'BigCommerce Trigger',
      color: '#0D52FF',
    },
    inputs: [],
    outputs: ['main'],
    credentials: [
      {
        name: 'bigcommerceApi',
        required: true,
      },
    ],
    webhooks: [
      {
        name: 'default',
        httpMethod: 'POST',
        responseMode: 'onReceived',
        path: 'default',
      },
    ],
    properties: [
      {
        displayName: 'Events',
        name: 'events',
        type: 'multiOptions',
        required: true,
        default: [],
        options: [ // transformed from https://developer.bigcommerce.com/api-docs/store-management/webhooks/webhook-events
          // Subscribe to all cart events. This will also subscribe you to cart/lineItem.
          { value: 'store/cart/*', name: 'store/cart/*' },
          // This webhook will fire whenever a new cart is created, either via a storefront shopper adding their first item to the cart, or when a new cart is created via an API consumer. If it is from the storefront, then it fires when the first product is added to a new session.(The cart did not exist before). For the API it means a POST to /carts, (V3 and Storefront API). The store/cart/updated hook will also fire.
          { value: 'store/cart/created', name: 'store/cart/created' },
          // This webhook is fired whenever a cart is modified through the changes in its line items. Eg. When a new item is added to a cart or an existing item’s quantity is updated. This hook also fires when the email is changed during guest checkout or when an existing item is deleted. The payload will include the ID of the cart being updated.
          { value: 'store/cart/updated', name: 'store/cart/updated' },
          // This webhook will fire whenever a cart is deleted. This will occur either when all items have been removed from a cart and it is auto-deleted, or when the cart is explicitly removed via a DELETE request by an API consumer. This ends the lifecycle of the cart. The store/cart/updated webhook will also fire when the last item is removed.
          { value: 'store/cart/deleted', name: 'store/cart/deleted' },
          // This webhook will fire whenever a new coupon code is applied to a cart. It will include the ID of the coupon code.
          { value: 'store/cart/couponApplied', name: 'store/cart/couponApplied' },
          // This webhook will fire once after a cart is abandoned. A cart is considered abandoned if no changes have been made for at least one hour after the last modified property. This hook is available for all store plans, regardless of whether the Abandoned Cart Saver feature is enabled.
          { value: 'store/cart/abandoned', name: 'store/cart/abandoned' },
          // This hook fires when a cart is converted into an order, which is typically after the payment step of checkout on the storefront. At this point, the cart is no longer accessible and has been deleted. This hook returns both the cart ID and order ID for correlation purposes.
          { value: 'store/cart/converted', name: 'store/cart/converted' },

          // Subscribe to all cart line item events. This webhook will fire when a change occurs to line items in the cart. This can be when items are added to a cart, removed or updated.(Ex. change to quantity, product options or price).
          { value: 'store/cart/lineItem/*', name: 'store/cart/lineItem/*' },
          // When a new item is added to the cart.
          { value: 'store/cart/lineItem/created', name: 'store/cart/lineItem/created' },
          // When an item’s quantity has changed or the product options change.
          { value: 'store/cart/lineItem/updated', name: 'store/cart/lineItem/updated' },
          // When an item is deleted from the cart.
          { value: 'store/cart/lineItem/deleted', name: 'store/cart/lineItem/deleted' },

          // Subscribe to all store/category events.
          { value: 'store/category/*', name: 'store/category/*' },
          // Category is created.
          { value: 'store/category/created', name: 'store/category/created' },
          // Category is updated.
          { value: 'store/category/updated', name: 'store/category/updated' },
          // Category is deleted.
          { value: 'store/category/deleted', name: 'store/category/deleted' },

          // Subscribe to all store/channel events.
          { value: 'store/channel/*', name: 'store/channel/*' },
          // Fires when a channel is created via control panel or API.
          { value: 'store/channel/created', name: 'store/channel/created' },
          // Fires when a channel is updated via control panel or API.
          { value: 'store/channel/updated', name: 'store/channel/updated' },

          // Subscribe to all store/customer events.
          { value: 'store/customer/*', name: 'store/customer/*' },
          // A new customer is created.
          { value: 'store/customer/created', name: 'store/customer/created' },
          // Customer is updated. Does not currently track changes to the customer address.
          { value: 'store/customer/updated', name: 'store/customer/updated' },
          // Customer is deleted.
          { value: 'store/customer/deleted', name: 'store/customer/deleted' },
          // Customer address is created.
          { value: 'store/customer/address/created', name: 'store/customer/address/created' },
          // Customer address is updated.
          { value: 'store/customer/address/updated', name: 'store/customer/address/updated' },
          // Customer address is deleted.
          { value: 'store/customer/address/deleted', name: 'store/customer/address/deleted' },
          // Customer default payment instrument is updated.
          { value: 'store/customer/payment/instrument/default/updated', name: 'store/customer/payment/instrument/default/updated' },

          // Subscribe to all store/order events.
          { value: 'store/order/*', name: 'store/order/*' },
          // Fires if an order is created using the control panel, an app or via the API.
          { value: 'store/order/created', name: 'store/order/created' },
          // Fires when an already created order is updated. Any changes to an existing order will fire this webhook. Updates can include changing the status, updating a coupon, or changing an address.
          { value: 'store/order/updated', name: 'store/order/updated' },
          // Order is archived.
          { value: 'store/order/archived', name: 'store/order/archived' },
          // This will only fire if the order status has changed, such as Pending to Awaiting Payment.
          { value: 'store/order/statusUpdated', name: 'store/order/statusUpdated' },
          // Order message is created by customer or in control panel.
          { value: 'store/order/message/created', name: 'store/order/message/created' },
          // A refund has been submitted against an order.
          { value: 'store/order/refund/created', name: 'store/order/refund/created' },
          // Fires when a transaction record is created.
          { value: 'store/order/transaction/created', name: 'store/order/transaction/created' },
          // Fires when a transaction record is updated.
          { value: 'store/order/transaction/updated', name: 'store/order/transaction/updated' },

          // Subscribe to all store/product events.
          { value: 'store/product/*', name: 'store/product/*' },
          // Product is deleted.
          { value: 'store/product/deleted', name: 'store/product/deleted' },
          // A new product is created.
          { value: 'store/product/created', name: 'store/product/created' },
          // Occurs when product details are edited. For a full list of product fields that trigger an updated event, see Product updated events below.
          { value: 'store/product/updated', name: 'store/product/updated' },
          // Product inventory is updated.
          { value: 'store/product/inventory/updated', name: 'store/product/inventory/updated' },
          // Fires if a product’s inventory is decremented or incremented, including when an order is placed. Webhook responds to inventory updates made using the control panel, CSV import, API or an app.
          { value: 'store/product/inventory/order/updated', name: 'store/product/inventory/order/updated' },

          // Subscribe to all store/shipment events.
          { value: 'store/shipment/*', name: 'store/shipment/*' },
          // Shipment is created.
          { value: 'store/shipment/created', name: 'store/shipment/created' },
          // Shipment is updated.
          { value: 'store/shipment/updated', name: 'store/shipment/updated' },
          // Shipment is deleted.
          { value: 'store/shipment/deleted', name: 'store/shipment/deleted' },

          // Subscribe to all store/sku events.
          { value: 'store/sku/*', name: 'store/sku/*' },
          // A new sku is created.
          { value: 'store/sku/created', name: 'store/sku/created' },
          // SKU is updated.
          { value: 'store/sku/updated', name: 'store/sku/updated' },
          // SKU is deleted.
          { value: 'store/sku/deleted', name: 'store/sku/deleted' },
          // SKU is updated.
          { value: 'store/sku/inventory/updated', name: 'store/sku/inventory/updated' },
          // This will fire when the inventory is updated via API, the control panel, when an order is placed and when an order is refunded and the inventory is returned to stock. This hook will fire based on a store’s Inventory settings.
          { value: 'store/sku/inventory/order/updated', name: 'store/sku/inventory/order/updated' },

          // Occurs when a client store is cancelled and uninstalled from the platform.
          { value: 'store/app/uninstalled', name: 'store/app/uninstalled' },
          // Occurs when changes are made to store settings. For a full list of fields that can trigger this event, see Store information updated events below.
          { value: 'store/information/updated', name: 'store/information/updated' },

          // Subscribe to all store/subscriber events.
          { value: 'store/subscriber/*', name: 'store/subscriber/*' },
          // Subscriber is created.
          { value: 'store/subscriber/created', name: 'store/subscriber/created' },
          // Subscriber is updated.
          { value: 'store/subscriber/updated', name: 'store/subscriber/updated' },
          // Subscriber is deleted.
          { value: 'store/subscriber/deleted', name: 'store/subscriber/deleted' },
        ],
      },
      {
        displayName: 'On Shutdown',
        name: 'onShutdown',
        type: 'options',
        options: [
          { value: 'delete', name: 'Delete BC Webhooks' },
          { value: 'inactivate', name: 'Inactivate BC Webhooks' },
        ],
        default: 'delete',
        description: 'What do do with BigCommerce webhooks when the workflow is deactivated',
      },
    ],
  };

  webhookMethods = {
    default: {
      // executes on activation or startup
      async checkExists(this: IHookFunctions): Promise<boolean> {
        const webhookData = this.getWorkflowStaticData('node');
        const webhookUrl = this.getNodeWebhookUrl('default');
        const events = this.getNodeParameter('events') as string[];
        const { data } = await bigcommerceApi.get(this, 'v3/hooks', {
          destination: webhookUrl,
        });

        const bcHooks: Record<string, IDataObject> = {}
        for (const webhook of data) {
          if (events.includes(webhook.scope)) {
            bcHooks[webhook.scope] = webhook;
          } else {
            await bigcommerceApi.del(this, `v3/hooks/${webhook.id}`);
          }
        }
        webhookData.bcHooks = bcHooks;

        for (const event of events) {
          if (!bcHooks[event]?.is_active) {
            return false;
          }
        }
        return true;
      },

      // executes if the above returns false
      async create(this: IHookFunctions): Promise<boolean> {
        const webhookUrl = this.getNodeWebhookUrl('default');
        const webhookData = this.getWorkflowStaticData('node');
        const events = this.getNodeParameter('events') as string[];
        const { bcHooks } = webhookData as { bcHooks: Record<string, IDataObject> };

        for (const event of events) {
          const bcHook = {
            scope: event,
            destination: webhookUrl,
            is_active: true,
            headers: {},
          }
          if (!bcHooks[event]) {
            const { data } = await bigcommerceApi.post(this, 'v3/hooks', bcHook);
            bcHooks[event] = data;
            console.log(`Created BigCommerce webhook: ${JSON.stringify(data)}`);
          } else if (!bcHooks[event].is_active) {
            const { data } = await bigcommerceApi.put(this, `v3/hooks/${bcHooks[event].id}`, bcHook);
            bcHooks[event] = data;
          }
        }
        return true;
      },

      // executes when deactivated or n8n is shutdown
      async delete(this: IHookFunctions): Promise<boolean> {
        const webhookData = this.getWorkflowStaticData('node');
        const { bcHooks } = webhookData as { bcHooks: Record<string, IDataObject> };
        const onShutdown = this.getNodeParameter('onShutdown');

        for (const bcHook of Object.values(bcHooks)) {
          try {
            if (onShutdown === 'delete') {
              const { data } = await bigcommerceApi.del(this, `v3/hooks/${bcHook.id}`);
              console.log(`Deleted BigCommerce webhook: ${JSON.stringify(data)}`);
            } else {
              // inactivate webhook
              await bigcommerceApi.put(this, `v3/hooks/${bcHook.id}`, {
                scope: bcHook.scope,
                destination: bcHook.destination,
                is_active: false,
                headers: bcHook.headers,
              });
            }
          } catch (error) {
            console.error(`Could not ${onShutdown} bc webhook: ${bcHook.id}: ${error}`);
            return false;
          }
        }
        delete webhookData.bcHooks;
        return true;
      },
    },
  };

  // handle data from request
  async webhook(this: IWebhookFunctions): Promise<IWebhookResponseData> {
    const req = this.getRequestObject();
    return {
      workflowData: [
        this.helpers.returnJsonArray(req.body),
      ],
    }
  }
}
