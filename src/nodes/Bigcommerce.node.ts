import { IExecuteFunctions } from 'n8n-core';
import {
  IDataObject,
  INodeExecutionData,
  INodeType,
  INodeTypeDescription,
  NodeOperationError,
} from 'n8n-workflow';

import { ResourceType } from '../resources';
import {
  bigcommerceApi,
  properties,
  resource,
  couponResources,
  customerResource,
  customerAddressResource,
  customerFormFieldValueResource,
  customerGroupResource,
  lineItemResource,
  orderResource,
  orderShippingAddressResource,
  orderShipmentResource,
  productResource,
  refundResource,
  subscriberResource,
  variantResource,
} from '../resources';
import { ExecuteFunctionOptions } from '../interfaces';

export class Bigcommerce implements INodeType {
  description: INodeTypeDescription = {
    displayName: 'BigCommerce',
    name: 'bigcommerce',
    group: ['transform'],
    version: 1,
    description: 'Allows requests to the Bigcommerce API',
    defaults: {
      name: 'Bigcommerce',
      color: '#0D52FF',
    },
    inputs: ['main'],
    outputs: ['main'],
    credentials: [
      {
        name: 'bigcommerceApi',
        required: true,
      },
    ],
    properties: [
      ...resource,
      ...couponResources.properties,
      ...customerResource.properties,
      ...customerAddressResource.properties,
      ...customerFormFieldValueResource.properties,
      ...customerGroupResource.properties,
      ...lineItemResource.properties,
      ...orderResource.properties,
      ...orderShippingAddressResource.properties,
      ...orderShipmentResource.properties,
      ...productResource.properties,
      ...refundResource.properties,
      ...subscriberResource.properties,
      ...variantResource.properties,
      ...properties,
    ]
  };

  async execute(this: IExecuteFunctions): Promise<INodeExecutionData[][]> {
    const items = this.getInputData();
    const returnData: IDataObject[] = [];

    for (let i = 0; i < items.length; i++) {
      const selectedResourceType = this.getNodeParameter('resource', i) as ResourceType;
      const operation = this.getNodeParameter('operation', i) as string;
      const options: ExecuteFunctionOptions = { operation, execute: this, i };
      let context;

      switch (selectedResourceType) {
        case ResourceType.COUPON:
          context = await couponResources.execute(options);
          break;
        case ResourceType.CUSTOMER:
          context = await customerResource.execute(options);
          break;
        case ResourceType.CUSTOMER_FORM_FIELD_VALUE:
          context = await customerFormFieldValueResource.execute(options);
          break;
        case ResourceType.CUSTOMER_ADDRESS:
          context = await customerAddressResource.execute(options);
          break;
        case ResourceType.CUSTOMER_GROUP:
          context = await customerGroupResource.execute(options);
          break;
        case ResourceType.LINE_ITEM:
          context = await lineItemResource.execute(options);
          break;
        case ResourceType.ORDER:
          context = await orderResource.execute(options);
          break;
        case ResourceType.ORDER_SHIPPING_ADDRESS:
          context = await orderShippingAddressResource.execute(options);
          break;
        case ResourceType.ORDER_SHIPMENT:
          context = await orderShipmentResource.execute(options);
          break;
        case ResourceType.PRODUCT:
          context = await productResource.execute(options);
          break;
        case ResourceType.REFUND:
          context = await refundResource.execute(options);
          break;
        case ResourceType.SUBSCRIBER:
          context = await subscriberResource.execute(options);
          break;
        case ResourceType.VARIANT:
          context = await variantResource.execute(options);
          break;
        default:
          throw new NodeOperationError(this.getNode(), `The resource "${selectedResourceType}" is not known!`);
      }

      const responseData = await bigcommerceApi.request(this, context);
      returnData.push(responseData);
    }

    return [this.helpers.returnJsonArray(returnData)];
  }
}
