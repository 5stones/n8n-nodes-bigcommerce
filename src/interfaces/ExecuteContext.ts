export interface ExecuteContext {
  endpoint: string;
  method: string;
  body?: any;
  qs?: any;
}
