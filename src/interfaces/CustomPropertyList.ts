import { CustomProperty } from './CustomProperty';

export interface CustomPropertyList {
  properties: CustomProperty[];
}
