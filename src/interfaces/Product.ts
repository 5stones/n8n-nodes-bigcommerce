export interface Product {
  product_id: number;
  quantity: number;
}
