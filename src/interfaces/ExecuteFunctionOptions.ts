import { IExecuteFunctions } from 'n8n-core';

export interface ExecuteFunctionOptions {
  operation: string,
  execute: IExecuteFunctions,
  i: number
};
