import { ExecuteFunctionOptions } from './ExecuteFunctionOptions';
import { ExecuteContext } from './ExecuteContext';

export type ExecuteFunction = (options: ExecuteFunctionOptions)
  => Promise<ExecuteContext>;
