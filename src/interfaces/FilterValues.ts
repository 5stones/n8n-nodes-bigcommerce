import { FilterValue } from './FilterValue';

export interface FilterValues {
  [key: string]: FilterValue[];
}
