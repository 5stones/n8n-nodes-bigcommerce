import { INodeProperties } from 'n8n-workflow';

import { ExecuteFunction } from './ExecuteFunction';

export interface Resource {
    readonly properties: INodeProperties[];
    execute: ExecuteFunction;
}
