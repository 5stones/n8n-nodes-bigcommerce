export interface CustomProperty {
  name: string;
  value: string;
}
