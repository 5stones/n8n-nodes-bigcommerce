export interface PostBody {
  [key: string]: object | number | undefined | null | string | boolean | string[] | object[];
  [key: number]: object | number | undefined | null | string | boolean | string[] | object[];
}
