import { Product } from './Product';

export interface ProductList {
	product: Product[];
}
