import { NodeParameterValue } from 'n8n-workflow';

export interface FilterValue {
  operation: string;
  value: NodeParameterValue;
}
