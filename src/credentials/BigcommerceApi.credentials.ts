import {
  ICredentialType,
  NodePropertyTypes,
} from 'n8n-workflow';


export class BigcommerceApi implements ICredentialType {
  name = 'bigcommerceApi';
  displayName = 'Bigcommerce Api';
  properties = [
    // The credentials to get from user and save encrypted.
    // Properties can be defined exactly in the same way
    // as node properties.
    {
      displayName: 'Store Hash',
      name: 'storeHash',
      type: 'string' as NodePropertyTypes,
      default: '',
    },
    {
      displayName: 'Access Token',
      name: 'token',
      type: 'string' as NodePropertyTypes,
      default: '',
    },
    {
      displayName: 'Client ID',
      name: 'client',
      type: 'string' as NodePropertyTypes,
      default: '',
    },
  ];
}
