import { Resource } from '../../interfaces';

import properties from './properties';
import { execute } from './execute';

export const product: Resource = {
  properties,
  execute,
};

export default product;
