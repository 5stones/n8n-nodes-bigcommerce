import { INodeProperties } from 'n8n-workflow';

import { operation } from './operation';
import { getAll } from './getAll';

export const product: INodeProperties[] = [
  operation,
  ...getAll,
];

export default product;
