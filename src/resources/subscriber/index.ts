import { Resource } from '../../interfaces';

import properties from './properties';
import { execute } from './execute';

export const subscriber: Resource = {
  properties,
  execute,
};

export default subscriber;
