import { INodeProperties } from 'n8n-workflow';

import { operation } from './operation';
import { getAll } from './getAll';
import { subscriberId } from './subscriberId';

export const subscriber: INodeProperties[] = [
  operation,
  ...subscriberId,
  ...getAll,
];

export default subscriber;
