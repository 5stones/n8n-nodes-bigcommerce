import { INodeProperties } from 'n8n-workflow';

import { Operation } from '../../Operation';
import { ResourceType } from '../../ResourceType';

export const subscriberId: INodeProperties[] = [
  {
    displayName: 'Subscriber Id',
    name: 'subscriber_id',
    type: 'string',
    default: '',
    description: '',
    displayOptions: {
      show: {
        operation: [
          Operation.UPDATE,
          Operation.GET,
        ],
        resource: [
          ResourceType.SUBSCRIBER,
        ],
      },
    },
  },
];
