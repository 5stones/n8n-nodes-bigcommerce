import { INodeProperties } from 'n8n-workflow';

import { ResourceType } from '../../ResourceType';
import { Operation } from '../../Operation';

export const operation: INodeProperties = {
    displayName: 'Operation',
    name: 'operation',
    type: 'options',
    displayOptions: {
        show: {
            resource: [
                ResourceType.SUBSCRIBER,
            ],
        },
    },
    options: [
        {
            name: 'Get All',
            value: Operation.GET_ALL,
            description: 'List subscribers',
        },
        {
          name: 'Get',
          value: Operation.GET,
          description: 'Get a subscriber',
        },
    ],
    default: Operation.GET_ALL,
    description: 'The operation to perform.',
};
