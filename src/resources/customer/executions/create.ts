import { IExecuteFunctions } from 'n8n-core';

import { PostBody } from '../../../interfaces';

export const create = async (
  execute: IExecuteFunctions,
  i: number,
): Promise<PostBody[]> => {
  return [
    {
      email: execute.getNodeParameter('email', i) as string,
      first_name: execute.getNodeParameter('first_name', i) as string,
      last_name: execute.getNodeParameter('last_name', i) as string,
      company: execute.getNodeParameter('company', i) as string,
      phone: String(execute.getNodeParameter('phone', i)) as string,
      notes: execute.getNodeParameter('notes', i) as string,
      tax_exempt_category: execute.getNodeParameter('tax_exempt_category', i) as string,
      customer_group_id: Number(execute.getNodeParameter('customer_group_id', i)),
    }
  ];
};
