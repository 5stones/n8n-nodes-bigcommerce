import { IExecuteFunctions } from 'n8n-core';
import { IDataObject } from 'n8n-workflow';

import { PostBody } from '../../../interfaces';

export const update = async (
  execute: IExecuteFunctions,
  i: number,
): Promise<PostBody[]> => {
  let body: PostBody[] = [{}];

  const properties = execute.getNodeParameter('properties', i, {}) as IDataObject;
  body[0]['id'] = Number(execute.getNodeParameter('id', i));

  for (const key of Object.keys(properties)) {
    body[0][key] = properties[key];
  }

  return body;
}
