import { IExecuteFunctions } from 'n8n-core';

import { CustomPropertyList, PostBody } from '../../../interfaces';

export const get = async (
  execute: IExecuteFunctions,
  i: number,
): Promise<PostBody> => {
  let qs: PostBody = {};
  const customProperties = execute.getNodeParameter('customProperties', i, {}) as CustomPropertyList;
  if(customProperties.properties) {
    for (const property of customProperties['properties']) {
      qs[property.name] = property.value;
    }
  }

  return qs;
}
