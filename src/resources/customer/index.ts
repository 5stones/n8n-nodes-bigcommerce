import { Resource } from '../../interfaces';

import properties from './properties';
import { execute } from './execute';

export const customer: Resource = {
  properties,
  execute,
};

export default customer;
