import { INodeProperties } from 'n8n-workflow';

import { create } from './create';
import { operation } from './operation';
import { update } from './update';

export const customer: INodeProperties[] = [
  operation,
  ...create,
  ...update,
];

export default customer;
