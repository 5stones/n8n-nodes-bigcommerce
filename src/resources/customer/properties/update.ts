import { INodeProperties } from 'n8n-workflow';

import { Operation } from '../../Operation';
import { ResourceType } from '../../ResourceType';

export const update: INodeProperties[] = [
  {
    displayName: 'User Id',
    name: 'id',
    type: 'string',
    default: '',
    description: 'Id of the updated customer',
    displayOptions: {
      show: {
        operation: [
          Operation.UPDATE,
        ],
        resource: [
          ResourceType.CUSTOMER,
        ],
      },
    },
  },
  {
    displayName: 'Properties',
    name: 'properties',
    type: 'collection',
    displayOptions: {
      show: {
        operation: [
          Operation.UPDATE,
        ],
        resource: [
          ResourceType.CUSTOMER,
        ],
      },
    },
    default: {},
    description: 'Properties to set on the updated user',
    placeholder: 'Add Property',
    options: [
      {
        displayName: 'First Name',
        name: 'first_name',
        type: 'string',
        default: '',
        description: 'The first name of the customer.',
      },
      {
        displayName: 'Last Name',
        name: 'last_name',
        type: 'string',
        default: '',
        description: 'The last name of the customer.',
      },
      {
        displayName: 'Email',
        name: 'email',
        type: 'string',
        default: '',
        description: 'The email address of the customer.',
      },
      {
        displayName: 'Phone',
        name: 'phone',
        type: 'string',
        default: '',
        description: 'The phone number of the customer.',
      },
      {
        displayName: 'Company',
        name: 'company',
        type: 'string',
        default: '',
        description: 'The company of the customer.',
      },
      {
        displayName: 'Notes',
        name: 'notes',
        type: 'string',
        default: '',
        description: 'Notes on the customer.',
      },
      {
        displayName: 'Tax exempt category',
        name: 'tax_exempt_category',
        type: 'string',
        default: '',
        description: 'The tax exempt category of the customer.',
      },
      {
        displayName: 'Group Id',
        name: 'customer_group_id',
        type: 'string',
        default: '',
        description: 'The group ID of the customer.',
      }
    ],
  },
];
