import { INodeProperties } from 'n8n-workflow';

import { Operation } from '../../Operation';
import { ResourceType } from '../../ResourceType';

export const operation: INodeProperties = {
    displayName: 'Operation',
    name: 'operation',
    type: 'options',
    displayOptions: {
      show: {
        resource: [
          ResourceType.CUSTOMER,
        ],
      },
    },
    options: [
      {
        name: 'Update',
        value: Operation.UPDATE,
        description: 'Update a customer',
      },
      {
        name: 'Create',
        value: Operation.CREATE,
        description: 'Create a customer',
      },
      {
        name: 'Get',
        value: Operation.GET,
        description: 'Get a customer',
      },
    ],
    default: Operation.UPDATE,
    description: 'The operation to perform.',
  };
