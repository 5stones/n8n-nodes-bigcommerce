import { INodeProperties } from 'n8n-workflow';

import { Operation } from '../../Operation';
import { ResourceType } from '../../ResourceType';

export const create: INodeProperties[] = [
    {
      displayName: 'First Name',
      name: 'first_name',
      type: 'string',
      default: '',
      description: 'The first name of the customer.',
      required: true,
      displayOptions: {
        show: {
          operation: [
            Operation.CREATE,
          ],
          resource: [
            ResourceType.CUSTOMER,
          ],
        },
      },
    },
    {
      displayName: 'Last Name',
      name: 'last_name',
      type: 'string',
      default: '',
      description: 'The last name of the customer.',
      required: true,
      displayOptions: {
        show: {
          operation: [
            Operation.CREATE,
          ],
          resource: [
            ResourceType.CUSTOMER,
          ],
        },
      },
    },
    {
      displayName: 'Email',
      name: 'email',
      type: 'string',
      default: '',
      description: 'The email address of the customer.',
      required: true,
      displayOptions: {
        show: {
          operation: [
            Operation.CREATE,
          ],
          resource: [
            ResourceType.CUSTOMER,
          ],
        },
      },
    },
    {
      displayName: 'Company',
      name: 'company',
      type: 'string',
      default: '',
      description: 'The company name of the customer.',
      displayOptions: {
        show: {
          operation: [
            Operation.CREATE,
          ],
          resource: [
            ResourceType.CUSTOMER,
          ],
        },
      },
    },
    {
      displayName: 'Phone',
      name: 'phone',
      type: 'string',
      default: '',
      description: 'The phone number of the customer.',
      displayOptions: {
        show: {
          operation: [
            Operation.CREATE,
          ],
          resource: [
            ResourceType.CUSTOMER,
          ],
        },
      },
    },
    {
      displayName: 'Notes',
      name: 'notes',
      type: 'string',
      default: '',
      description: 'Custom notes on the customer.',
      displayOptions: {
        show: {
          operation: [
            Operation.CREATE,
          ],
          resource: [
            ResourceType.CUSTOMER,
          ],
        },
      },
    },
    {
      displayName: 'Tax Exempt Category',
      name: 'tax_exempt_category',
      type: 'string',
      default: '',
      description: 'The tax exempt category of the customer.',
      displayOptions: {
        show: {
          operation: [
            Operation.CREATE,
          ],
          resource: [
            ResourceType.CUSTOMER,
          ],
        },
      },
    },
    {
      displayName: 'Customer Group ID',
      name: 'customer_group_id',
      type: 'number',
      default: '',
      description: 'The group ID of the customer.',
      displayOptions: {
        show: {
          operation: [
            Operation.CREATE,
          ],
          resource: [
            ResourceType.CUSTOMER,
          ],
        },
      },
    },
    {
      displayName: 'Address 1',
      name: 'address1',
      type: 'string',
      default: '',
      description: 'The first line of the customer\'s address.',
      required: true,
      displayOptions: {
        show: {
          operation: [
            Operation.CREATE,
          ],
          resource: [
            ResourceType.CUSTOMER,
          ],
        },
      },
    },
    {
      displayName: 'Address 2',
      name: 'address2',
      type: 'string',
      default: '',
      description: 'The second line of the customer\'s address.',
      displayOptions: {
        show: {
          operation: [
            Operation.CREATE,
          ],
          resource: [
            ResourceType.CUSTOMER,
          ],
        },
      },
    },
    {
      displayName: 'City',
      name: 'city',
      type: 'string',
      default: '',
      description: 'The city of the customer\'s address.',
      required: true,
      displayOptions: {
        show: {
          operation: [
            Operation.CREATE,
          ],
          resource: [
            ResourceType.CUSTOMER,
          ],
        },
      },
    },
    {
      displayName: 'Country (ISO 2)',
      name: 'country',
      type: 'string',
      default: '',
      description: 'The ISO 2 format country of the customer\'s address.',
      required: true,
      displayOptions: {
        show: {
          operation: [
            Operation.CREATE,
          ],
          resource: [
            ResourceType.CUSTOMER,
          ],
        },
      },
    },
    {
      displayName: 'State',
      name: 'state',
      type: 'string',
      default: '',
      description: 'The state or province of the customer\'s address. (Use full name of state ie \'New York\')',
      required: true,
      displayOptions: {
        show: {
          operation: [
            Operation.CREATE,
          ],
          resource: [
            ResourceType.CUSTOMER,
          ],
        },
      },
    },
    {
      displayName: 'Zip Code',
      name: 'postal_code',
      type: 'string',
      default: '',
      description: 'The zip code of the customer\'s address.',
      required: true,
      displayOptions: {
        show: {
          operation: [
            Operation.CREATE,
          ],
          resource: [
            ResourceType.CUSTOMER,
          ],
        },
      },
    },
    {
      displayName: 'Address Type',
      name: 'address_type',
      type: 'options',
      displayOptions: {
        show: {
          operation: [
            Operation.CREATE,
          ],
          resource: [
            ResourceType.CUSTOMER,
          ],
        },
      },
      options: [
        {
          name: 'Residential',
          value: 'residential',
          description: '',
        },
        {
          name: 'Commercial',
          value: 'commercial',
          description: '',
        },
      ],
      default: 'residential',
      description: 'The type of address.',
    },
];
