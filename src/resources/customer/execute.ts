import {
  ExecuteFunction,
  ExecuteFunctionOptions,
  ExecuteContext,
} from '../../interfaces';
import { Operation } from '../Operation';
import {
  create,
  get,
  update,
} from './executions';

export const execute: ExecuteFunction = async ({
  operation,
  execute,
  i,
}: ExecuteFunctionOptions): Promise<ExecuteContext> => {
  let response;
  let method = 'GET';
  let body = {};
  let qs = {};

  switch (operation) {
    case Operation.UPDATE:
      // update customer
      body = await update(execute, i);
      method = 'PUT';
      break;
    case Operation.CREATE:
      // create customer
      body = await create(execute, i);
      method = 'POST';
      break;
    case Operation.GET:
      // get customer
      qs = await get(execute, i);
      break;
    default:
      throw new Error(`The operation "${operation}" is not known!`);
  }

  return {
    endpoint: `v3/customers`,
    method,
    body,
    qs,
  };
};
