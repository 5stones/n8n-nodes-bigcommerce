// import { } from 'n8n-core';
import { NodeApiError, IDataObject, IExecuteFunctions, IWebhookFunctions, IHookFunctions  } from 'n8n-workflow';

import { ExecuteContext } from '../interfaces';

type IFunctions = IExecuteFunctions | IWebhookFunctions | IHookFunctions;

export async function request(f: IFunctions, context: ExecuteContext) {
  const credentials = await f.getCredentials('bigcommerceApi');
  if (credentials === undefined) {
    throw new Error('No credentials could be found!');
  }

  const baseUrl = `https://api.bigcommerce.com/stores/${credentials.storeHash}`;

  try {
    return await f.helpers.request({
      method: context.method,
      body: context.body,
      qs: context.qs,
      uri: `${baseUrl}/${context.endpoint}`,
      headers: {
        'X-Auth-Token': credentials.token,
        ...credentials.client && {
          'X-Auth-Client': credentials.client,
        },
      },
      json: true,
    });
  } catch (error: any) {
    throw new NodeApiError(f.getNode(), error);
  }
}

export async function get(f: IFunctions, endpoint: string, qs?: IDataObject) {
  return request(f, { method: 'GET', endpoint, qs });
}

export async function post(f: IFunctions, endpoint: string, body?: IDataObject) {
  return request(f, { method: 'POST', endpoint, body });
}

export async function put(f: IFunctions, endpoint: string, body?: IDataObject) {
  return request(f, { method: 'PUT', endpoint, body });
}

export async function del(f: IFunctions, endpoint: string, qs?: IDataObject) {
  return request(f, { method: 'DELETE', endpoint, qs });
}
