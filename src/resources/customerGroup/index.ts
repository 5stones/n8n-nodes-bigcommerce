import { Resource } from '../../interfaces';

import properties from './properties';
import { execute } from './execute';

export const customerGroup: Resource = {
  properties,
  execute,
};

export default customerGroup;
