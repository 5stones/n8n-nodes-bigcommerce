import { INodeProperties } from 'n8n-workflow';

import { get } from './get';
import { operation } from './operation';

export const customerGroup: INodeProperties[] = [
  operation,
  ...get,
];

export default customerGroup;
