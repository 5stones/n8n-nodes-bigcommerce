import { INodeProperties } from 'n8n-workflow';

import { Operation } from '../../Operation';
import { ResourceType } from '../../ResourceType';

export const get: INodeProperties[] = [
  {
    displayName: 'Customer Group Id',
    name: 'id',
    type: 'number',
    default: '',
    description: 'Id of customer group',
    displayOptions: {
      show: {
        operation: [
          Operation.GET,
        ],
        resource: [
          ResourceType.CUSTOMER_GROUP,
        ],
      },
    },
  },
];
