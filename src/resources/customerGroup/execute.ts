import {
	ExecuteFunction,
	ExecuteFunctionOptions,
	ExecuteContext,
} from '../../interfaces';
import { Operation } from '../Operation';

export const execute: ExecuteFunction = async ({
	operation,
	execute,
	i,
}: ExecuteFunctionOptions): Promise<ExecuteContext> => {
  let endpoint = `v2/customer_groups`;
  switch (operation) {
    case Operation.GET:
      // get customer group
      const id = Number(execute.getNodeParameter('id', i));

      endpoint += `/${id}`;
      break;
    default:
      throw new Error(`The operation "${operation}" is not known!`);
  }
  return {
		endpoint,
		method: `GET`,
	};
};
