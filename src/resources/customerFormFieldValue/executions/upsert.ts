import { IExecuteFunctions } from 'n8n-core';

import { PostBody } from '../../../interfaces';

export const upsert = async (
  execute: IExecuteFunctions,
  i: number,
): Promise<PostBody[]> => {
  const body: PostBody[] = [
    {
      name: execute.getNodeParameter('fieldName', i) as string,
      customer_id: Number(execute.getNodeParameter('id', i)),
    },
  ];

  const fieldValueType = execute.getNodeParameter('fieldValueType', i) as string;
  if (fieldValueType === 'checkbox') {
    body[0].value = [] as string[];
    const values = execute.getNodeParameter('fieldValueCheckbox', i) as string;
    const valuesArray = values.split(',');

    for (const value of valuesArray) {
      //@ts-ignore
      body[0].value.push(value as string);
    }
  }
  if (fieldValueType === 'textField') {
    body[0].value = execute.getNodeParameter('fieldValueText', i) as string;
  }

  return body;
}
