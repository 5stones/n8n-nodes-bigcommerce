import { INodeProperties } from 'n8n-workflow';

import { Operation } from '../../Operation';
import { ResourceType } from '../../ResourceType';

export const properties: INodeProperties[] = [
  {
    displayName: 'User Id',
    name: 'id',
    type: 'string',
    default: '',
    description: 'Id of the updated customer',
    displayOptions: {
      show: {
        operation: [
          Operation.GET,
          Operation.UPSERT,
        ],
        resource: [
          ResourceType.CUSTOMER_FORM_FIELD_VALUE,
        ],
      },
    },
  },
  {
    displayName: 'Field Name',
    name: 'fieldName',
    type: 'string',
    default: '',
    description: 'Name of the field being fetched',
    displayOptions: {
      show: {
        operation: [
          Operation.GET,
          Operation.UPSERT,
        ],
        resource: [
          ResourceType.CUSTOMER_FORM_FIELD_VALUE,
        ],
      },
    },
  },
  {
    displayName: 'Field Value Type',
    name: 'fieldValueType',
    type: 'options',
    displayOptions: {
      show: {
        operation: [
          Operation.UPSERT,
        ],
        resource: [
          ResourceType.CUSTOMER_FORM_FIELD_VALUE,
        ],
      },
    },
    options: [
      {
        name: 'Checkbox',
        value: 'checkbox',
        description: 'The Account Signup Field type chosen in bigcommerce was checkbox',
      },
      {
        name: 'Text Field',
        value: 'textField',
        description: 'The Account Signup Field type chosen in bigcommerce was text field'
      }
      // TODO Add more as needed
    ],
    default: Operation.GET,
    description: 'The operation to perform.',
  },
  {
    displayName: 'Field Value (Comma separated list)',
    name: 'fieldValueCheckbox',
    type: 'string',
    default: '',
    description: 'Value of the field',
    displayOptions: {
      show: {
        operation: [
          Operation.UPSERT,
        ],
        resource: [
          ResourceType.CUSTOMER_FORM_FIELD_VALUE,
        ],
        fieldValueType: [
          'checkbox',
        ],
      },
    },
  },
  {
    displayName: 'Field Value (Text Field)',
    name: 'fieldValueText',
    type: 'string',
    default: '',
    description: 'Value of the field',
    displayOptions: {
      show: {
        operation: [
          Operation.UPSERT,
        ],
        resource: [
          ResourceType.CUSTOMER_FORM_FIELD_VALUE,
        ],
        fieldValueType: [
          'textField',
        ],
      },
    },
  },
];
