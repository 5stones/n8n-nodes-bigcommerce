import { INodeProperties } from 'n8n-workflow';

import { Operation } from '../../Operation';
import { ResourceType } from '../../ResourceType';

export const operation: INodeProperties = {
    displayName: 'Operation',
    name: 'operation',
    type: 'options',
    displayOptions: {
      show: {
        resource: [
          ResourceType.CUSTOMER_FORM_FIELD_VALUE,
        ],
      },
    },
    options: [
      {
        name: 'Get',
        value: Operation.GET,
        description: 'Get a customer',
      },
      {
        name: 'Upsert',
        value: Operation.UPSERT,
        description: 'Upsert a customer field value to a customer'
      }
    ],
    default: Operation.GET,
    description: 'The operation to perform.',
  };
