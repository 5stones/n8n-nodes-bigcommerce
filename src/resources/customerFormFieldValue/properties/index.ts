import { INodeProperties } from 'n8n-workflow';

import { operation } from './operation';
import { properties } from './properties';

export const customerFormFieldValue: INodeProperties[] = [
  operation,
  ...properties,
];

export default customerFormFieldValue;
