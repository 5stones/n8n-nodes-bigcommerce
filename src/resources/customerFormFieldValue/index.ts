import { Resource } from '../../interfaces';

import properties from './properties';
import { execute } from './execute';

export const customerFormFieldValue: Resource = {
  properties,
  execute,
};

export default customerFormFieldValue;
