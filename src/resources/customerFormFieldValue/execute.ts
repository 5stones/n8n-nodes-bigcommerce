import {
	ExecuteFunction,
	ExecuteFunctionOptions,
	ExecuteContext,
} from '../../interfaces';
import { Operation } from '../Operation';

import { upsert } from './executions';

export const execute: ExecuteFunction = async ({
	operation,
	execute,
	i,
}: ExecuteFunctionOptions): Promise<ExecuteContext> => {
	let method = `GET`;
	let qs: any = {};
	let body: any = {};

  switch (operation) {
    case Operation.GET:
      // get customer form field values
      qs['customer_id'] = execute.getNodeParameter('id', i) as string;
      qs['field_name'] = execute.getNodeParameter('fieldName', i) as string;
      break;
    case Operation.UPSERT:
      // upsert customer form field values
      body = await upsert(execute, i);
      method = 'PUT';
      break;
    default:
      throw new Error(`The operation "${operation}" is not known!`);
  }

  return {
		endpoint: `v3/customers/form-field-values`,
		method,
		body,
		qs,
	}
};
