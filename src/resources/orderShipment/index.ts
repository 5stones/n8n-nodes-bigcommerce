import { Resource } from '../../interfaces';

import properties from './properties';
import { execute } from './execute';

export const orderShipment: Resource = {
  properties,
  execute,
};

export default orderShipment;
