import {
	ExecuteFunction,
	ExecuteFunctionOptions,
	ExecuteContext,
} from '../../interfaces';
import { Operation } from '../Operation';

export const execute: ExecuteFunction = async ({
	operation,
	execute,
	i,
}: ExecuteFunctionOptions): Promise<ExecuteContext> => {
    let endpoint = `v2/orders`;
    let method = `GET`;

    switch (operation) {
    case Operation.GET:
      // get order shipping addresses
      const orderId = execute.getNodeParameter('order_id', i) as string;

      endpoint += `/${orderId}/shipments`;
      break;
    default:
      throw new Error(`The operation "${operation}" is not known!`);
    }
    return {
    	endpoint,
    	method,
    };
}
