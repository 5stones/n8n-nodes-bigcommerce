import { INodeProperties } from 'n8n-workflow';
import { ResourceType } from './ResourceType';

export { coupon as couponResources } from './coupon';
export { customer as customerResource } from './customer';
export { customerAddress as customerAddressResource } from './customerAddress';
export { customerFormFieldValue as customerFormFieldValueResource } from './customerFormFieldValue';
export { customerGroup as customerGroupResource } from './customerGroup';
export { lineItem as lineItemResource } from './lineItem';
export { order as orderResource } from './order';
export { orderShippingAddress as orderShippingAddressResource } from './orderShippingAddress';
export { orderShipment as orderShipmentResource } from './orderShipment';
export { product as productResource } from './product';
export { refund as refundResource } from './refund';
export { subscriber as subscriberResource } from './subscriber';
export { variant as variantResource } from './variant';

export { ResourceType } from './ResourceType';
export { Operation } from './Operation';
export { properties } from './properties';
export * as bigcommerceApi from './bigcommerceApi';

export const resource: INodeProperties[] = [
  {
    displayName: 'ResourceType',
    name: 'resource',
    type: 'options',
    options: [
      {
        name: 'Coupon',
        value: ResourceType.COUPON,
      },
      {
        name: 'Customer',
        value: ResourceType.CUSTOMER,
      },
      {
        name: 'Customer Form Field Values',
        value: ResourceType.CUSTOMER_FORM_FIELD_VALUE,
      },
      {
        name: 'Customer Address',
        value: ResourceType.CUSTOMER_ADDRESS,
      },
      {
        name: 'Customer Group',
        value: ResourceType.CUSTOMER_GROUP,
      },
      {
        name: 'Line Items',
        value: ResourceType.LINE_ITEM,
      },
      {
        name: 'Order',
        value: ResourceType.ORDER,
      },
      {
        name: 'Order Shipping Address',
        value: ResourceType.ORDER_SHIPPING_ADDRESS,
      },
      {
        name: 'Order Shipment',
        value: ResourceType.ORDER_SHIPMENT,
      },
      {
        name: 'Product',
        value: ResourceType.PRODUCT,
      },
      {
        name: 'Refunds',
        value: ResourceType.REFUND,
      },
      {
        name: 'Subscriber',
        value: ResourceType.SUBSCRIBER,
      },
      {
        name: 'Variant',
        value: ResourceType.VARIANT,
      }
    ],
    default: ResourceType.CUSTOMER,
    description: 'The resource to operate on.',
  }
]
