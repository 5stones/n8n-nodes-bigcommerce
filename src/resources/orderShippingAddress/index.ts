import { Resource } from '../../interfaces';

import properties from './properties';
import { execute } from './execute';

export const orderShippingAddress: Resource = {
  properties,
  execute,
};

export default orderShippingAddress;
