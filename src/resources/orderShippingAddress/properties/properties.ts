import { INodeProperties } from 'n8n-workflow';

import { ResourceType } from '../../ResourceType';

export const properties: INodeProperties[] = [
  {
    displayName: 'Order Id',
    name: 'order_id',
    type: 'string',
    default: '',
    description: '',
    displayOptions: {
      show: {
        resource: [
          ResourceType.ORDER_SHIPPING_ADDRESS,
        ],
      },
    },
  },
];
