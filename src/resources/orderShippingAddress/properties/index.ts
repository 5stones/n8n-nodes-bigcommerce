import { INodeProperties } from 'n8n-workflow';

import { operation } from './operation';
import { properties } from './properties';

export const orderShippingAddress: INodeProperties[] = [
  operation,
  ...properties,
];

export default orderShippingAddress;
