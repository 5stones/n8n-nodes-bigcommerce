import { INodeProperties } from 'n8n-workflow';

import { RefundOperation } from './refund';
import { Operation } from './Operation';
import { ResourceType } from './ResourceType';

export const properties: INodeProperties[] = [
  {
    displayName: 'Custom Properties',
    name: 'customProperties',
    placeholder: 'Add Custom Property',
    description: 'Adds a custom property. (Make sure to use bigcommerce api docs as a guide)',
    type: 'fixedCollection',
    typeOptions: {
      multipleValues: true,
    },
    default: {},
    displayOptions: {
      show: {
        operation: [
          Operation.CREATE,
          Operation.UPDATE,
          Operation.GET,
          Operation.GET_ALL,
        ],
        resource: [
          ResourceType.ORDER,
          ResourceType.CUSTOMER,
          ResourceType.CUSTOMER_ADDRESS,
          ResourceType.PRODUCT,
          ResourceType.REFUND,
          ResourceType.SUBSCRIBER,
          ResourceType.VARIANT,
        ],
      },
    },
    options: [
      {
        name: 'properties',
        displayName: 'Properties',
        values: [
          {
            displayName: 'Property Name',
            name: 'name',
            type: 'string',
            default: '',
            description: 'Name of the property to set.',
          },
          {
            displayName: 'Property Value',
            name: 'value',
            type: 'string',
            default: '',
            description: 'Value of the property to set.',
          },
        ],
      },
    ],
  },
]
