import { INodeProperties } from 'n8n-workflow';

import { orderId } from './orderId';
import { create } from './create';
import { getAll } from './getAll';
import { operation } from './operation';

export const order: INodeProperties[] = [
  operation,
  ...orderId,
  ...create,
  ...getAll,
];

export default order;
