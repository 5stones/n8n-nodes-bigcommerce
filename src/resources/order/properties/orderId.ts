import { INodeProperties } from 'n8n-workflow';

import { Operation } from '../../Operation';
import { OrderOperation } from '../OrderOperation';
import { ResourceType } from '../../ResourceType';

export const orderId: INodeProperties[] = [
  {
    displayName: 'Order Id',
    name: 'order_id',
    type: 'string',
    default: '',
    description: '',
    displayOptions: {
      show: {
        operation: [
          OrderOperation.ARCHIVE,
          Operation.UPDATE,
          Operation.GET,
        ],
        resource: [
          ResourceType.ORDER,
        ],
      },
    },
  },
];
