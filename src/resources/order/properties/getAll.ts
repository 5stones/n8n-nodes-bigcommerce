import { INodeProperties } from 'n8n-workflow';

import { Operation } from '../../Operation';
import { ResourceType } from '../../ResourceType';

export const getAll: INodeProperties[] = [
  {
    displayName: 'Page',
    name: 'page',
    type: 'number',
    default: 1,
    description: 'Page of data',
    typeOptions: {
      maxValue: 100,
      minValue: 1,
      numberStepSize: 1,
    },
    displayOptions: {
      show: {
        operation: [
          Operation.GET_ALL,
        ],
        resource: [
          ResourceType.ORDER,
        ],
      },
    },
  },
  {
    displayName: 'Limit',
    name: 'limit',
    type: 'number',
    default: 25,
    description: 'Limit amount of orders',
    typeOptions: {
      maxValue: 250,
      minValue: 25,
      numberStepSize: 25,
    },
    displayOptions: {
      show: {
        operation: [
          Operation.GET_ALL,
        ],
        resource: [
          ResourceType.ORDER,
        ],
      },
    },
  }
];
