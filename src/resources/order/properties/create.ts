import { INodeProperties } from 'n8n-workflow';

import { Operation } from '../../Operation';
import { ResourceType } from '../../ResourceType';

export const create: INodeProperties[] = [
  {
    displayName: 'First Name',
    name: 'first_name',
    type: 'string',
    default: '',
    description: 'First name in the billing address',
    displayOptions: {
      show: {
        operation: [
          Operation.CREATE,
        ],
        resource: [
          ResourceType.ORDER,
        ],
      },
    },
  },
  {
    displayName: 'Last Name',
    name: 'last_name',
    type: 'string',
    default: '',
    description: 'Last name in the billing address',
    displayOptions: {
      show: {
        operation: [
          Operation.CREATE,
        ],
        resource: [
          ResourceType.ORDER,
        ],
      },
    },
  },
  {
    displayName: 'Company',
    name: 'company',
    type: 'string',
    default: '',
    description: 'Company in the billing address',
    displayOptions: {
      show: {
        operation: [
          Operation.CREATE,
        ],
        resource: [
          ResourceType.ORDER,
        ],
      },
    },
  },
  {
    displayName: 'Street 1',
    name: 'street_1',
    type: 'string',
    default: '',
    description: 'Street 1 in the billing address',
    displayOptions: {
      show: {
        operation: [
          Operation.CREATE,
        ],
        resource: [
          ResourceType.ORDER,
        ],
      },
    },
  },
  {
    displayName: 'Street 2',
    name: 'street_2',
    type: 'string',
    default: '',
    description: 'Street 2 in the billing address',
    displayOptions: {
      show: {
        operation: [
          Operation.CREATE,
        ],
        resource: [
          ResourceType.ORDER,
        ],
      },
    },
  },
  {
    displayName: 'City',
    name: 'city',
    type: 'string',
    default: '',
    description: 'City in the billing address',
    displayOptions: {
      show: {
        operation: [
          Operation.CREATE,
        ],
        resource: [
          ResourceType.ORDER,
        ],
      },
    },
  },
  {
    displayName: 'State',
    name: 'state',
    type: 'string',
    default: '',
    description: 'State in the billing address',
    displayOptions: {
      show: {
        operation: [
          Operation.CREATE,
        ],
        resource: [
          ResourceType.ORDER,
        ],
      },
    },
  },
  {
    displayName: 'Zip',
    name: 'zip',
    type: 'string',
    default: '',
    description: 'Zip in the billing address',
    displayOptions: {
      show: {
        operation: [
          Operation.CREATE,
        ],
        resource: [
          ResourceType.ORDER,
        ],
      },
    },
  },
  {
    displayName: 'Country',
    name: 'country',
    type: 'string',
    default: 'US',
    description: 'Country in the billing address (ISO 2)',
    displayOptions: {
      show: {
        operation: [
          Operation.CREATE,
        ],
        resource: [
          ResourceType.ORDER,
        ],
      },
    },
  },
  {
    displayName: 'Phone',
    name: 'phone',
    type: 'string',
    default: '',
    description: 'Phone in the billing address',
    displayOptions: {
      show: {
        operation: [
          Operation.CREATE,
        ],
        resource: [
          ResourceType.ORDER,
        ],
      },
    },
  },
  {
    displayName: 'Email',
    name: 'email',
    type: 'string',
    default: '',
    description: 'Email in the billing address',
    displayOptions: {
      show: {
        operation: [
          Operation.CREATE,
        ],
        resource: [
          ResourceType.ORDER,
        ],
      },
    },
  },
  {
    displayName: 'Customer Id',
    name: 'customer_id',
    type: 'string',
    default: '',
    description: 'Customer to be placed on order',
    displayOptions: {
      show: {
        operation: [
          Operation.CREATE,
        ],
        resource: [
          ResourceType.ORDER,
        ],
      },
    },
  },
  {
    displayName: 'Products Array (JSON)',
    name: 'productsArray',
    description: 'Add an array of products formated as [{ product_id: (id), quantity: (quantity) }]',
    type: 'json',
    displayOptions: {
      show: {
        operation: [
          Operation.CREATE,
        ],
        resource: [
          ResourceType.ORDER,
        ],
      },
    },
    default: '',
  },
  {
    displayName: 'Products',
    name: 'products',
    placeholder: 'Add a Product',
    description: 'Products in the order',
    type: 'fixedCollection',
    typeOptions: {
      multipleValues: true,
    },
    displayOptions: {
      show: {
        operation: [
          Operation.CREATE,
        ],
        resource: [
          ResourceType.ORDER,
        ],
      },
    },
    default: {},
    options: [
      {
        displayName: 'Product',
        name: 'product',
        values: [
          {
            displayName: 'Product Id',
            name: 'product_id',
            type: 'string',
            default: '',
          },
          {
            displayName: 'Quantity',
            name: 'quantity',
            type: 'number',
            typeOptions: {
              maxValue: 10,
              minValue: 1,
              numberStepSize: 1,
            },
            default: 1,
          }
        ]
      },
    ],
  },
];
