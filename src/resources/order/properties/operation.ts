import { INodeProperties } from 'n8n-workflow';

import { Operation } from '../../Operation';
import { ResourceType } from '../../ResourceType';
import { OrderOperation } from '../OrderOperation';

export const operation: INodeProperties = {
  displayName: 'Operation',
  name: 'operation',
  type: 'options',
  displayOptions: {
    show: {
      resource: [
        ResourceType.ORDER,
      ],
    },
  },
  options: [
    {
      name: 'Create',
      value: Operation.CREATE,
      description: 'Create an order',
    },
    {
      name: 'Get',
      value: Operation.GET,
      description: 'Get order by id',
    },
    {
      name: 'Get All',
      value: Operation.GET_ALL,
      description: 'Get all orders with query',
    },
    {
      name: 'Archive',
      value: OrderOperation.ARCHIVE,
      description: 'Archive an order',
    },
    {
      name: 'Update',
      value: Operation.UPDATE,
      description: 'Update an order',
    },
  ],
  default: Operation.CREATE,
  description: 'The operation to perform.',
};
