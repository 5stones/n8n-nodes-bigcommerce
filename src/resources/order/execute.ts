import {
	ExecuteFunction,
	ExecuteFunctionOptions,
	ExecuteContext,
} from '../../interfaces';
import { Operation } from '../Operation';
import { OrderOperation } from './OrderOperation';
import {
	create,
	getAll,
	update,
} from './executions';

export const execute: ExecuteFunction = async ({
	operation,
	execute,
	i,
}: ExecuteFunctionOptions): Promise<ExecuteContext> => {
	let method = `GET`;
	let endpoint = `v2/orders`;
	let body;
	let qs;
  let orderId;

  switch (operation) {
    case Operation.UPDATE:
      // update order
      orderId = execute.getNodeParameter('order_id', i) as string;
			endpoint += `/${orderId}`;
			method = 'PUT';
      body = await update(execute, i);
      break;
    case Operation.CREATE:
      // create order
      body = await create(execute, i);
      method = 'POST';
      break;
    case Operation.GET:
      // get order
      orderId = execute.getNodeParameter('order_id', i) as string;
      endpoint += `/${orderId}`;
      break;
    case Operation.GET_ALL:
      // get orders by query
      qs = await getAll(execute, i);
      break;
    case OrderOperation.ARCHIVE:
      // archive order
      orderId = execute.getNodeParameter('order_id', i) as string;
      method = 'DELETE';
      endpoint += `/${orderId}`;
      break;
    default:
      throw new Error(`The operation "${operation}" is not known!`);
  }
  return {
		endpoint,
		method,
		body,
		qs,
	}
}
