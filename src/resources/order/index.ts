import { Resource } from '../../interfaces';

import properties from './properties';
import { execute } from './execute';

export { OrderOperation } from './OrderOperation';

export const order: Resource = {
  properties,
  execute,
};

export default order;
