import { IExecuteFunctions } from 'n8n-core';

import { CustomPropertyList, PostBody } from '../../../interfaces';

export const update = async (
  execute: IExecuteFunctions,
  i: number,
): Promise<PostBody> => {
  let body: PostBody = {};
  const customProperties = execute.getNodeParameter('customProperties', i, {}) as CustomPropertyList;
  for (const property of customProperties['properties']) {
    body[property.name] = property.value;
  }
  const orderId = execute.getNodeParameter('order_id', i) as string;

  return body;
}
