import { IExecuteFunctions } from 'n8n-core';

import { CustomPropertyList, PostBody, ProductList, Product } from '../../../interfaces';

export const create = async (
  execute: IExecuteFunctions,
  i: number,
): Promise<PostBody> => {
  let products = [];
  const productsList = execute.getNodeParameter('products', i, {}) as ProductList;
  if(productsList['product']) {
    for (const productObj of productsList['product']) {
      products.push({
        product_id: productObj.product_id,
        quantity: productObj.quantity,
      });
    }
  }

  const productsArray = execute.getNodeParameter('productsArray', i) as Product[];
  if (productsArray) {
    products = products.concat(productsArray);
  }

  const body: PostBody = {
    products: products,
    customer_id: execute.getNodeParameter('customer_id', i) as string,
    billing_address:	{
      first_name: execute.getNodeParameter('first_name', i) as string,
      last_name: execute.getNodeParameter('last_name', i) as string,
      company: execute.getNodeParameter('company', i) as string,
      street_1: execute.getNodeParameter('street_1', i) as string,
      street_2: execute.getNodeParameter('street_2', i) as string,
      city: execute.getNodeParameter('city', i) as string,
      state: execute.getNodeParameter('state', i) as string,
      zip: String(execute.getNodeParameter('zip', i)) as string,
      country_iso2: execute.getNodeParameter('country', i) as string,
      phone: String(execute.getNodeParameter('phone', i)) as string,
      email: execute.getNodeParameter('email', i) as string
    },
  };

  const customProperties = execute.getNodeParameter('customProperties', i, {}) as CustomPropertyList;
  if(customProperties['properties']) {
    for (const property of customProperties['properties']) {
      body[property.name] = property.value;
    }
  }

  return body;
}
