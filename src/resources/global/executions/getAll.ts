import { IExecuteFunctions } from 'n8n-core';
import { IDataObject } from 'n8n-workflow';

import { CustomPropertyList, PostBody, ProductList } from '../../../interfaces';

export const getAll = async (execute: IExecuteFunctions, i: number): Promise<PostBody> => {
  let qs: PostBody = {};
  const page = execute.getNodeParameter('page', i) as IDataObject;
  const limit = execute.getNodeParameter('limit', i) as IDataObject;
  qs.page = page;
  qs.limit = limit;

  const customProperties = execute.getNodeParameter('customProperties', i, {}) as CustomPropertyList;
  if(customProperties && customProperties['properties']) {
    for (const property of customProperties['properties']) {
      qs[property.name] = property.value;
    }
  }

  return qs;
};
