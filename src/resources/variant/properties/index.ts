import { INodeProperties } from 'n8n-workflow';

import { operation } from './operation';
import { getAll } from './getAll';

export const variant: INodeProperties[] = [
  operation,
  ...getAll,
];

export default variant;
