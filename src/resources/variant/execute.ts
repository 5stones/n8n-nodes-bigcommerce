import {
  ExecuteFunction,
  ExecuteFunctionOptions,
  ExecuteContext,
} from '../../interfaces';
import { Operation } from '../Operation';
import { getAll } from './executions';

export const execute: ExecuteFunction = async ({
  operation,
  execute,
  i,
}: ExecuteFunctionOptions): Promise<ExecuteContext> => {
  let endpoint = `v3/catalog/variants`;
  let response;
  let method = 'GET';
  let body = {};
  let qs = {};

  switch (operation) {
    case Operation.GET_ALL:
      // update customer
      qs = await getAll(execute, i);
      break;
    default:
      throw new Error(`The operation "${operation}" is not known!`);
  }

  return {
    endpoint,
    method,
    body,
    qs,
  };
};
