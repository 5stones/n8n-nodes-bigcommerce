import { Resource } from '../../interfaces';

import properties from './properties';
import { execute } from './execute';

export const variant: Resource = {
  properties,
  execute,
};

export default variant;
