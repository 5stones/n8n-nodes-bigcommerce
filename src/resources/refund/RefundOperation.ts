export enum RefundOperation {
  GET_ALL = 'getAll',
  GET_FOR_ORDER = 'getForOrder',
  CREATE = 'create',
  CREATE_QUOTE = 'createQuote',
}
