import { INodeProperties } from "n8n-workflow";

import { RefundOperation } from "../RefundOperation";
import { ResourceType } from "../../ResourceType";

const resource = [ResourceType.REFUND];

export const properties: INodeProperties[] = [
  {
    displayName: "Order Id",
    name: "order_id",
    type: "string",
    default: "",
    description: "",
    displayOptions: {
      show: {
        operation: [RefundOperation.GET_FOR_ORDER, RefundOperation.CREATE, RefundOperation.CREATE_QUOTE],
        resource,
      },
    },
  },

  {
    displayName: "Reason",
    name: "reason",
    type: "string",
    default: "",
    description: "Reason for refund",
    displayOptions: {
      show: {
        operation: [RefundOperation.CREATE],
        resource,
      },
    },
  },
  {
    displayName: "Items (JSON)",
    name: "items",
    description:
      "Add an array of items formated as [{ item_type: PRODUCT|SHIPPING|HANDLING|GIFT_WRAPPING|ORDER, item_id?: number, ... }]",
    type: "json",
    displayOptions: {
      show: {
        operation: [RefundOperation.CREATE, RefundOperation.CREATE_QUOTE],
        resource,
      },
    },
    default: "",
  },
  {
    displayName: "Payments (JSON)",
    name: "payments",
    description:
      "Add an array of payments formated as [{ provider_id?: string, amount?: number, offline?: boolean }]",
    type: "json",
    displayOptions: {
      show: {
        operation: [RefundOperation.CREATE],
        resource,
      },
    },
    default: "",
  },
];
