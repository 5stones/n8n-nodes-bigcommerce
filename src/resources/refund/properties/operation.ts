import { INodeProperties } from "n8n-workflow";

import { ResourceType } from "../../ResourceType";
import { RefundOperation } from "../RefundOperation";
import { Operation } from "../../Operation";

export const operation: INodeProperties = {
  displayName: "Operation",
  name: "operation",
  type: "options",
  displayOptions: {
    show: {
      resource: [ResourceType.REFUND],
    },
  },
  options: [
    {
      name: "Get",
      value: Operation.GET_ALL,
      description: "Get refunds with query",
    },
    {
      name: "Get For Order",
      value: RefundOperation.GET_FOR_ORDER,
      description: "Get refunds for one Order",
    },
    {
      name: "Create",
      value: RefundOperation.CREATE,
      description: "Create refunds for an Order",
    },
    {
      name: "Create Quote",
      value: RefundOperation.CREATE_QUOTE,
      description: "Create refund quotes for an Order",
    },
  ],
  default: Operation.GET_ALL,
  description: "The operation to perform.",
};
