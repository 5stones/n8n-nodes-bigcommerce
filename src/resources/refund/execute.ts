import {
  ExecuteFunction,
  ExecuteFunctionOptions,
  ExecuteContext,
  CustomPropertyList,
} from "../../interfaces";
import { RefundOperation } from "./RefundOperation";

export const execute: ExecuteFunction = async (
  options: ExecuteFunctionOptions
): Promise<ExecuteContext> => {
  if (!isRefundOperation(options.operation)) {
    throw new Error(`The operation "${options.operation}" is not known!`);
  }
  return refundOperations[options.operation](options);
};

const refundOperations: Record<RefundOperation, ExecuteFunction> = {
  [RefundOperation.GET_FOR_ORDER]: async ({ execute, i }) => {
    const orderId = execute.getNodeParameter("order_id", i) as string;
    return {
      method: `GET`,
      endpoint: `v3/orders/${orderId}/payment_actions/refunds`,
    };
  },

  [RefundOperation.GET_ALL]: async ({ execute, i }) => {
    const qs = mapProperties(
      execute.getNodeParameter("customProperties", i, {}) as CustomPropertyList
    );
    return {
      method: `GET`,
      endpoint: `v3/orders/payment_actions/refunds`,
      qs,
    };
  },

  [RefundOperation.CREATE_QUOTE]: async ({ execute, i }) => {
    const orderId = String(execute.getNodeParameter("order_id", i));
    return {
      method: `POST`,
      endpoint: `v3/orders/${orderId}/payment_actions/refund_quotes`,
      body: {
        items: execute.getNodeParameter("items", i) || undefined,
      },
    };
  },

  [RefundOperation.CREATE]: async ({ execute, i }) => {
    const orderId = String(execute.getNodeParameter("order_id", i));
    return {
      method: `POST`,
      endpoint: `v3/orders/${orderId}/payment_actions/refunds`,
      body: {
        reason: execute.getNodeParameter("reason", i) || undefined,
        items: execute.getNodeParameter("items", i) || undefined,
        payments: execute.getNodeParameter("payments", i) || undefined,
      },
    };
  },
};

function isRefundOperation(v: string): v is RefundOperation {
  return Object.values(RefundOperation).includes(v as RefundOperation);
}

function mapProperties(
  customProperties: CustomPropertyList
): Record<string, string> {
  return Object.fromEntries(
    customProperties.properties.map(({ name, value }) => [name, value])
  );
}
