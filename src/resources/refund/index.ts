import { Resource } from '../../interfaces';

import properties from './properties';
import { execute } from './execute';

export { RefundOperation } from './RefundOperation';

export const refund: Resource = {
  properties,
  execute,
};

export default refund;
