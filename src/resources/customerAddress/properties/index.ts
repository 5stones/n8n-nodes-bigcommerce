import { INodeProperties } from 'n8n-workflow';

import { operation } from './operation';

export const customerAddress: INodeProperties[] = [
  operation,
];

export default customerAddress;
