import { INodeProperties } from 'n8n-workflow';

import { Operation } from '../../Operation';
import { ResourceType } from '../../ResourceType';

export const operation: INodeProperties = {
  displayName: 'Operation',
  name: 'operation',
  type: 'options',
  displayOptions: {
    show: {
      resource: [
        ResourceType.CUSTOMER_ADDRESS,
      ],
    },
  },
  options: [
    {
      name: 'Get',
      value: Operation.GET,
      description: 'Get customer address',
    },
  ],
  default: Operation.GET,
  description: 'The operation to perform.',
};
