import { Resource } from '../../interfaces';

import properties from './properties';
import { execute } from './execute';

export const customerAddress: Resource = {
  properties,
  execute,
};

export default customerAddress;
