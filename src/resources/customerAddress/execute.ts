import { IDataObject } from 'n8n-workflow';

import {
	ExecuteFunction,
	ExecuteFunctionOptions,
	ExecuteContext,
	CustomPropertyList,
} from '../../interfaces';
import { Operation } from '../Operation';

export const execute: ExecuteFunction = async ({
	operation,
	execute,
	i,
}: ExecuteFunctionOptions): Promise<ExecuteContext> => {
	let qs: any = {};

  switch (operation) {
    case Operation.GET:
      // get customer addresses
      const customProperties = execute.getNodeParameter('customProperties', i, {}) as CustomPropertyList;
      for (const property of customProperties['properties']) {
        qs[property.name] = property.value;
      }
      break;
    default:
      throw new Error(`The operation "${operation}" is not known!`);
  }

  return {
		method: `GET`,
		endpoint: `v3/customers/addresses`,
		qs,
	};
}
