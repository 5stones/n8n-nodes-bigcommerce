import { Resource } from '../../interfaces';

import properties from './properties';
import { execute } from './execute';

export const lineItem: Resource = {
  properties,
  execute,
};

export default lineItem;
