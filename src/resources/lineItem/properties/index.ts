import { INodeProperties } from 'n8n-workflow';

import { operation } from './operation';
import { get } from './get';

export const lineItem: INodeProperties[] = [
  operation,
  ...get,
];

export default lineItem;
