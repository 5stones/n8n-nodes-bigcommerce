import { INodeProperties } from 'n8n-workflow';

import { Operation } from '../../Operation';
import { ResourceType } from '../../ResourceType';

export const get: INodeProperties[] = [
  {
    displayName: 'Order Id',
    name: 'order_id',
    type: 'string',
    default: '',
    description: 'Order to get Line Items from',
    required: true,
    displayOptions: {
      show: {
        operation: [
          Operation.GET,
        ],
        resource: [
          ResourceType.LINE_ITEM,
        ],
      },
    },
  },
  {
    displayName: 'Limit',
    name: 'limit',
    type: 'number',
    typeOptions: {
      minValue: 1,
      numberStepSize: 1,
    },
    default: 20,
    description: 'Limit of line items to return',
    displayOptions: {
      show: {
        operation: [
          Operation.GET,
        ],
        resource: [
          ResourceType.LINE_ITEM,
        ],
      },
    },
  },
  {
    displayName: 'Page',
    name: 'page',
    type: 'number',
    typeOptions: {
      minValue: 1,
      numberStepSize: 1,
    },
    default: 1,
    description: 'Page of line items to return',
    displayOptions: {
      show: {
        operation: [
          Operation.GET,
        ],
        resource: [
          ResourceType.LINE_ITEM,
        ],
      },
    },
  },
];
