import {
	ExecuteFunction,
	ExecuteFunctionOptions,
	ExecuteContext,
} from '../../interfaces';
import { Operation } from '../Operation';

export const execute: ExecuteFunction = async ({
	operation,
	execute,
	i,
}: ExecuteFunctionOptions): Promise<ExecuteContext> => {
  let endpoint = `v3/customers/addresses`;
	let qs: any = {};

  switch (operation) {
    case Operation.GET:
      // get line items
      const orderId = execute.getNodeParameter('order_id', i) as string;
      endpoint = `v2/orders/${orderId}/products`;
      qs.limit = execute.getNodeParameter('limit', i) as number;
      qs.page = execute.getNodeParameter('page', i) as number;
      break;
    default:
      throw new Error(`The operation "${operation}" is not known!`);
  }
  return {
		endpoint,
		method: `GET`,
		qs,
	};
};
