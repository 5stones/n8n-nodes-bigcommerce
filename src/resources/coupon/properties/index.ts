import { INodeProperties } from 'n8n-workflow';

import { operation } from './operation';
import { get } from './get';

export const coupon: INodeProperties[] = [
  operation,
  ...get,
];

export default coupon;
