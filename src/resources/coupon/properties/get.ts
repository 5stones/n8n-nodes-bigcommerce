import { INodeProperties } from 'n8n-workflow';

import { Operation } from '../../Operation';
import { ResourceType } from '../../ResourceType';

export const get: INodeProperties[] = [
  {
    displayName: 'Order Id',
    name: 'order_id',
    type: 'string',
    default: '',
    description: 'Order from which to get Coupons',
    required: true,
    displayOptions: {
      show: {
        operation: [
          Operation.GET,
        ],
        resource: [
          ResourceType.COUPON,
        ],
      },
    },
  },
];
