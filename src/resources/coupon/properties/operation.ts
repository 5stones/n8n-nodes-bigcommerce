import { INodeProperties } from 'n8n-workflow';

import { Operation } from '../../Operation';
import { ResourceType } from '../../ResourceType';

export const operation: INodeProperties = {
    displayName: 'Operation',
    name: 'operation',
    type: 'options',
    displayOptions: {
      show: {
        resource: [
          ResourceType.COUPON,
        ],
      },
    },
    options: [
      {
        name: 'Get',
        value: Operation.GET,
        description: 'Get a coupon',
      },
    ],
    default: Operation.GET,
    description: 'The operation to perform.',
  };
