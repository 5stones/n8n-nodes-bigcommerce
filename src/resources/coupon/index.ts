import { Resource } from '../../interfaces';

import properties from './properties';
import { execute } from './execute';

export const coupon: Resource = {
  properties,
  execute,
};

export default coupon;
