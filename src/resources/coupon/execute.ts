import {
  ExecuteFunction,
  ExecuteFunctionOptions,
  ExecuteContext,
} from '../../interfaces';
import { Operation } from '../Operation';
import { get } from './executions';

export const execute: ExecuteFunction = async ({
  operation,
  execute,
  i,
}: ExecuteFunctionOptions) => {
  let method = 'GET';
  let endpoint = 'v2/orders';

  switch (operation) {
    case Operation.GET:
      // get coupon
      const orderId = execute.getNodeParameter('order_id', i) as string;
      endpoint += `/${orderId}/coupons`;

      break;
    default:
      throw new Error(`The operation "${operation}" is not known!`);
  }

  return {
    endpoint,
    method,
  };
};
