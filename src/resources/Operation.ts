export enum Operation {
  CREATE = 'create',
  UPDATE = 'update',
  GET = 'get',
  GET_ALL = 'getAll',
  UPSERT = 'upsert',
}
